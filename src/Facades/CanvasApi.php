<?php

namespace Uncgits\CanvasApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class CanvasApi extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CanvasApi'; // the IoC binding.
    }
}
