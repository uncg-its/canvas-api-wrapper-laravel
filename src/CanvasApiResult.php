<?php

namespace Uncgits\CanvasApiLaravel;

use Uncgits\CanvasApi\CanvasApiResult as BaseApiResult;

/**
 * Represents a set of results from the API, obtained via one or more API calls
 * Extended from base class to support caching
 */
class CanvasApiResult extends BaseApiResult
{
    /*
    |--------------------------------------------------------------------------
    | Properties
    |--------------------------------------------------------------------------
    */

    /**
     * The source of the API call, 'api' or 'cache'
     *
     * @var string
     */
    protected $source = 'api';

    /**
     * The Cache Key for this result
     *
     * @var string|null
     */
    protected $cacheKey = null;

    /*
    |--------------------------------------------------------------------------
    | Getters
    |--------------------------------------------------------------------------
    */

    /**
     * Get the source of the API call, 'api' or 'cache'
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Get the Cache Key for this result
     *
     * @return string|null
     */
    public function getCacheKey()
    {
        return $this->cacheKey;
    }

    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    */


    /**
     * Set the source of the API call, 'api' or 'cache'
     *
     * @param string  $source  The source of the API call, 'api' or 'cache'
     *
     * @return  self
     */
    public function setSource(string $source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * Set the Cache Key for this result
     *
     * @param  string|null  $cacheKey  The Cache Key for this result
     *
     * @return  self
     */
    public function setCacheKey($cacheKey)
    {
        $this->cacheKey = $cacheKey;
        return $this;
    }
}
