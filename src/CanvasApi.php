<?php

namespace Uncgits\CanvasApiLaravel;

use Uncgits\CanvasApi\CanvasApiEndpoint;
use Uncgits\CanvasApi\CanvasApi as BaseApi;
use Uncgits\CanvasApiLaravel\Events\ApiRequestStarted;
use Uncgits\CanvasApi\Clients\CanvasApiClientInterface;
use Uncgits\CanvasApiLaravel\Events\ApiRequestFinished;

class CanvasApi extends BaseApi
{
    protected $cacheBypassed = false;

    public function __construct()
    {
        // set default adapter if specified
        $defaultAdapter = config('canvas-api.defaults.adapter');
        if (!empty($defaultAdapter)) {
            $this->setAdapter($defaultAdapter);

            // if we have default adapter, set default config if specified
            $defaultConfigKey = config('canvas-api.defaults.config');
            if (!empty($defaultConfigKey)) {
                $this->setConfig(config('canvas-api.configs.' . $defaultConfigKey . '.class'));
            }
        }
    }

    public function execute(CanvasApiClientInterface $client, CanvasApiEndpoint $endpoint, $method, $arguments)
    {
        event(new ApiRequestStarted($client, $endpoint, $method, $arguments));

        if (!$this->isCacheable($client, $endpoint, $method, $arguments)) {
            $result = new CanvasApiResult(
                $this->adapter->usingConfig($this->config)->transaction($endpoint),
                $this->config
            );
            $result->setSource('api');

            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
            return $result;
        }

        // figure out cache key
        $cacheKey = 'canvas-api-' . hash('sha3-512', serialize($client) . serialize($endpoint) . $method . json_encode($arguments));

        // check for the cached result first
        if (!is_null($cached = \Cache::get($cacheKey))) {
            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, unserialize($cached)));
            return unserialize($cached)
                ->setSource('cache')
                ->setCacheKey($cacheKey);
        }

        // wasn't cached, so execute the call
        $result = new CanvasApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint), $this->config);

        if ($result->getStatus() === 'success') {
            // it should be cached if it was successful
            \Cache::put($cacheKey, serialize($result), now()->addMinutes(config('canvas-api.cache_minutes', 10)));
        }

        event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
        return $result->setCacheKey($cacheKey);
    }

    public function withoutCache()
    {
        $this->cacheBypassed = true;
        return $this;
    }

    // caching helpers

    private function isCacheable(CanvasApiClientInterface $client, CanvasApiEndpoint $endpoint, $method, $arguments)
    {
        // reset temp cache bypass
        $bypass = $this->cacheBypassed;
        $this->cacheBypassed = false;

        if ($bypass) {
            return false;
        }

        if (!config('canvas-api.cache_active')) {
            return false;
        }

        if (strtolower($endpoint->getMethod()) !== 'get') {
            return false;
        }

        $cachedCallsForClient = config('canvas-api.cacheable_calls.' . get_class($client));
        if (is_null($cachedCallsForClient)) {
            return false;
        }

        if ($cachedCallsForClient !== ['*'] && !in_array($method, $cachedCallsForClient)) {
            return false;
        }

        return true;
    }
}
