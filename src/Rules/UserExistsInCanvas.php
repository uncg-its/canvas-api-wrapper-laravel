<?php

namespace Uncgits\CanvasApiLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;

class UserExistsInCanvas implements Rule
{
    private $failedUser;

    protected $loginType;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($loginType = 'canvas_id')
    {
        $this->loginType = $loginType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = $value;

        if ($this->loginType === 'sis_login_id') {
            $user = 'sis_login_id:' . $user;
        }

        if ($this->loginType === 'sis_user_id') {
            $user = 'sis_user_id:' . $user;
        }

        $getUserResult = \CanvasApi::using('users')->getUser($user);


        if ($getUserResult->getStatus() === 'success') {
            return true;
        }

        $this->failedUser = $value;
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User ' . $this->failedUser . ' does not exist in this Canvas environment (login type: ' . $this->loginType . ')';
    }
}
