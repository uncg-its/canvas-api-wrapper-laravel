<?php

namespace Uncgits\CanvasApiLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;

class CourseExistsInCanvas implements Rule
{
    private $failedCourseId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $getCourseResult = \CanvasApi::using('courses')->getCourse($value);

        if ($getCourseResult->getStatus() === 'success') {
            return true;
        }

        $this->failedCourseId = $value;
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Course ' . $this->failedCourseId . ' does not exist in this Canvas environment';
    }
}
